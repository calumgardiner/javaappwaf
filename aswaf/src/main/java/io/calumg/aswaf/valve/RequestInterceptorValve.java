package io.calumg.aswaf.valve;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;
import org.apache.commons.text.StringEscapeUtils;

import io.calumg.aswaf.issue.ParameterSecurityIssue;
import io.calumg.aswaf.issue.ParameterSecurityIssueMatcherFactory;
import io.calumg.aswaf.parameter.HttpParameter;

public class RequestInterceptorValve extends ValveBase {

	private final ParameterSecurityIssueMatcherFactory issueMatcherFactory = new ParameterSecurityIssueMatcherFactory();

	@Override
	public void invoke(final Request request, final Response response) throws IOException, ServletException {

		final List<ParameterSecurityIssue> issuesFound = new ArrayList<ParameterSecurityIssue>();

		getParameters(request).forEach(param -> assessParameter(param, issuesFound));

		final boolean safe = issuesFound.isEmpty();

		if (safe) {
			this.getNext().invoke(request, response);
		} else {
			response.getResponse().setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.getResponse().resetBuffer();
			response.getResponse().getWriter().write("<h1>Security issues found with request:</h1>");
			response.getResponse().getWriter().write("<ul>");
			for (final ParameterSecurityIssue issue : issuesFound) {
				response.getResponse().getWriter()
						.write(String.format("<li>ParameterKey %s , ParameterValue %s , %s</li>",
								StringEscapeUtils.escapeHtml4(issue.getParameter().getName()),
								StringEscapeUtils.escapeHtml4(issue.getParameter().getValue()),
								StringEscapeUtils.escapeHtml4(issue.getIssueMatcher().getReason())));
			}
			response.getResponse().getWriter().write("</ul>");
		}
	}

	private List<HttpParameter> getParameters(final Request request) {
		final List<HttpParameter> parameters = new ArrayList<HttpParameter>();
		final Enumeration<?> nameEnumeration = request.getParameterNames();
		while (nameEnumeration.hasMoreElements()) {
			final String parameterName = (String) nameEnumeration.nextElement();
			final String parameterValue = request.getParameter(parameterName);
			parameters.add(new HttpParameter(parameterName, parameterValue));
		}
		return parameters;
	}

	private void assessParameter(final HttpParameter parameter, final List<ParameterSecurityIssue> issuesFound) {
		this.issueMatcherFactory.getMatchers().stream()
				// Apply matchers to parameter
				.filter(matcher -> matcher.match(parameter))
				// Collect matcher hits
				.collect(Collectors.toList())
				// Create issues and add to found list
				.forEach(matcher -> issuesFound.add(new ParameterSecurityIssue(parameter, matcher)));
	}
}
