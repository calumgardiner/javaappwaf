package io.calumg.aswaf.issue;

import io.calumg.aswaf.issue.matchers.ParameterSecurityIssueMatcher;
import io.calumg.aswaf.parameter.HttpParameter;

public class ParameterSecurityIssue {

	private final HttpParameter parameter;
	private final ParameterSecurityIssueMatcher issueMatcher;

	public ParameterSecurityIssue(final HttpParameter parameter, final ParameterSecurityIssueMatcher issueMatcher) {
		this.parameter = parameter;
		this.issueMatcher = issueMatcher;
	}

	public HttpParameter getParameter() {
		return parameter;
	}

	public ParameterSecurityIssueMatcher getIssueMatcher() {
		return issueMatcher;
	}

}