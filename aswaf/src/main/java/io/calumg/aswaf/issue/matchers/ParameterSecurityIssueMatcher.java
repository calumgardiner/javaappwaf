package io.calumg.aswaf.issue.matchers;

import io.calumg.aswaf.parameter.HttpParameter;

public interface ParameterSecurityIssueMatcher {
	
	static final String REASON_FORMAT = "[%s] found potential issue: %s.";
	
	public boolean match(final HttpParameter parameter);
	
	public String getReason();

}
