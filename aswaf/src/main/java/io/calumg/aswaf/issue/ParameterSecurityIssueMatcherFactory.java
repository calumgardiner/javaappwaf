package io.calumg.aswaf.issue;

import java.util.Arrays;
import java.util.List;

import io.calumg.aswaf.issue.matchers.ParameterRegexIssueMatcher;
import io.calumg.aswaf.issue.matchers.ParameterSecurityIssueMatcher;

public class ParameterSecurityIssueMatcherFactory {
	
	public List<ParameterSecurityIssueMatcher> getMatchers() {
		return Arrays.asList(new ParameterRegexIssueMatcher("<script>", "Basic XSS"));
	}

}
