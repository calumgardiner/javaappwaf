package io.calumg.aswaf.issue.matchers;

import java.util.regex.Pattern;

import io.calumg.aswaf.parameter.HttpParameter;

public class ParameterRegexIssueMatcher implements ParameterSecurityIssueMatcher {

	private final String regexp;
	private final Pattern pattern;
	private final String reason;

	public ParameterRegexIssueMatcher(final String regexp, final String reason) {
		this.regexp = regexp;
		this.pattern = Pattern.compile(regexp);
		final String matchReason = String.format("%s caused by parameter matching known pattern '%s'", reason, regexp);
		this.reason = String.format(REASON_FORMAT, ParameterRegexIssueMatcher.class.getName(), matchReason);
	}

	@Override
	public boolean match(final HttpParameter parameter) {
		boolean matches = false;
		if (parameter.getValue() != null) {
			matches = this.pattern.matcher(parameter.getValue()).find();
		}
		return matches;
	}

	@Override
	public String getReason() {
		return this.reason;
	}

	public String getRegexp() {
		return regexp;
	}

}
