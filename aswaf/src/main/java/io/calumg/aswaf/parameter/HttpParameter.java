package io.calumg.aswaf.parameter;

public final class HttpParameter {

	private final String parameterName;
	private final String parameterValue;

	public HttpParameter(final String parameterName, final String parameterValue) {
		this.parameterName = parameterName;
		this.parameterValue = parameterValue;
	}

	public String getName() {
		return parameterName;
	}

	public String getValue() {
		return parameterValue;
	}

}
